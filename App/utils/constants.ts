export const colors = {
  red: '#ff0000',
  green: '#00ff00',
  orange: '#FF7700',
  blue: '#0000ff',
  black: '#000000',
  white: '#ffffff',
};
