import {StyleSheet} from 'react-native';
import {colors} from '../../utils/constants';

export const makeStyles = () => {
  return StyleSheet.create({
    container: {
      display: 'flex',
      alignItems: 'center',
      padding: 5,
    },
    titleContainer: {
      marginVertical: 15,
    },
    titleApp: {
      color: colors.red,
      fontSize: 45,
      textDecorationLine: 'underline',
    },
    textInputContainer: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-around',
      width: '100%',
      marginVertical: 15,
    },
    textInputNewTask: {
      width: '70%',
      borderColor: colors.blue,
      borderWidth: 1,
      fontSize: 25,
      fontWeight: '600',
      textAlign: 'center',
    },
    flatListStyle: {
      height: '75%',
    },
    listItemsContainer: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-around',
      marginVertical: 10,
    },
    itemContainer: {
      display: 'flex',
      justifyContent: 'center',
      padding: 10,
      width: '80%',
      backgroundColor: '#FFEECC',
      borderColor: colors.orange,
      borderWidth: 2,
      borderRadius: 5,
    },
    itemText: {
      color: colors.black,
      fontSize: 20,
      fontWeight: '600',
      textTransform: 'capitalize',
    },
  });
};
