import React from 'react';
import {View, Text} from 'react-native';
import {makeStyles} from './styles';

const index = () => {
  const styles = makeStyles();
  return (
    <View style={styles.emptyListContainer}>
      <Text style={styles.emptyListText}>The list is empty</Text>
    </View>
  );
};

export default index;
