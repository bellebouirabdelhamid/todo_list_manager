import {StyleSheet} from 'react-native';
import {colors} from '../../../../utils/constants';

export const makeStyles = () => {
  return StyleSheet.create({
    emptyListContainer: {
      marginVertical: 15,
      display: 'flex',
      padding: 10,
      width: '90%',
      backgroundColor: '#FFEECC',
      borderColor: colors.orange,
      borderWidth: 2,
      borderRadius: 5,
    },
    emptyListText: {
      fontSize: 20,
      fontWeight: '600',
      textAlign: 'center',
    },
  });
};
