import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import {makeStyles} from './styles';

type ButtonProps = {
  clickButton: () => void;
};

const DeleteTaskButton = ({clickButton}: ButtonProps) => {
  const styles = makeStyles();
  return (
    <TouchableOpacity
      onPress={clickButton}
      style={styles.deleteButtonContainer}>
      <Text style={styles.deleteButtonText}>-</Text>
    </TouchableOpacity>
  );
};

export default DeleteTaskButton;
