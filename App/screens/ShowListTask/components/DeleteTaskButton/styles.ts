import {StyleSheet} from 'react-native';
import {colors} from '../../../../utils/constants';

export const makeStyles = () => {
  return StyleSheet.create({
    deleteButtonContainer: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: colors.red,
      width: '15%',
      borderRadius: 5,
    },
    deleteButtonText: {
      color: colors.white,
      fontSize: 30,
      fontWeight: '700',
    },
  });
};
