import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import {makeStyles} from './styles';

type AddTaskButtonProps = {
  textButton: String;
  clickButton: () => void;
};

const AddTaskButton = ({textButton, clickButton}: AddTaskButtonProps) => {
  const styles = makeStyles();
  return (
    <TouchableOpacity style={styles.buttonContainer} onPress={clickButton}>
      <Text style={styles.buttonText}>{textButton}</Text>
    </TouchableOpacity>
  );
};

export default AddTaskButton;
