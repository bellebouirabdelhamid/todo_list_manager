import {StyleSheet} from 'react-native';

export const makeStyles = () => {
  return StyleSheet.create({
    buttonContainer: {
      width: '15%',
      backgroundColor: '#4682A9',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      padding: 10,
      borderRadius: 5,
    },
    buttonText: {
      color: '#fff',
      fontSize: 30,
      fontWeight: '700',
    },
  });
};
