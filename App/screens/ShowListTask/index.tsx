import React, {useState} from 'react';
import {Text, TextInput, SafeAreaView, View, FlatList} from 'react-native';
import {makeStyles} from './styles';
import AddTaskButton from './components/AddButton';
import DeleteTaskButton from './components/DeleteTaskButton';
import EmptyList from './components/EmptyList';

const ShowListTask = () => {
  let [newTaskToAdd, setNewTaskToAdd] = useState('');
  let [listOfTasks, setListOfTasks] = useState<String[]>([]);
  let addNewTaskToTheList = (param: string | String) => {
    setListOfTasks([...listOfTasks, param]);
    setNewTaskToAdd('');
  };
  const styles = makeStyles();
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.titleContainer}>
        <Text style={styles.titleApp}>To do list</Text>
      </View>
      <View style={styles.textInputContainer}>
        <TextInput
          placeholder="Enter new task"
          style={styles.textInputNewTask}
          onChangeText={txt => setNewTaskToAdd(txt)}
          value={newTaskToAdd}
        />
        <AddTaskButton
          textButton="+"
          clickButton={() => addNewTaskToTheList(newTaskToAdd)}
        />
      </View>
      {listOfTasks.length !== 0 ? (
        <View style={styles.flatListStyle}>
          <FlatList
            data={listOfTasks}
            renderItem={currentTask => {
              return (
                <View style={styles.listItemsContainer}>
                  <View style={styles.itemContainer}>
                    <Text style={styles.itemText}>{currentTask.item}</Text>
                  </View>
                  <DeleteTaskButton
                    clickButton={() =>
                      setListOfTasks(
                        listOfTasks.filter(
                          itemToDelete => itemToDelete !== currentTask.item,
                        ),
                      )
                    }
                  />
                </View>
              );
            }}
          />
        </View>
      ) : (
        <EmptyList />
      )}
    </SafeAreaView>
  );
};

export default ShowListTask;
